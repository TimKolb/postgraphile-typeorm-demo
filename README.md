# PostGraphile TypeORM Demo 

[![pipeline status](https://gitlab.com/TimKolb/postgraphile-typeorm-demo/badges/master/pipeline.svg)](https://gitlab.com/TimKolb/postgraphile-typeorm-demo/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## How to use this blueprint

1.  🏃 Run the setup script
1.  ✏️ Implement models
1.  🛠 Configure postgraphile
1.  ⛅ Deploy to production

## Setup

Run the setup script

```bash
$ ./cli setup
usage: cli [options] [flags]
  options:
    s|setup                       install dependencies and configure development environment
        --clean                   clean interactively all files not tracked by git !CAUTION!
        --force                   force file override
        --debug                   enable debug mode for this script

  flags:
    --help                        display this help
```

and start the application

```bash
docker-compose up
```

You can visualize it with GraphQL Voyager:

-   visit Voyager Mutation http://localhost:3000/voyager/m
-   visit Voyager Query http://localhost:3000/voyager/q

or run queries:

-   visit GraphiQL http://localhost:3000/graphiql

Create a TypeORM Model in `<root>/src/entity/${modelName}.ts`

Update the database schema with:

```bash
docker-compose run --rm app yarn run sync
```

## Purpose of this project

The project goal is to enable a developer to:

-   Define entities in GraphQL language and generate TypeORM Typescript files
-   Configure postgraphile according to your needs

## License

MIT

## Author

Tim Kolberger
[<img src="https://simpleicons.org/icons/twitter.svg" width="24"/>](https://twitter.com/TimKolberger)
